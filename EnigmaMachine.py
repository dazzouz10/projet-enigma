# EnigmaMachine

from string import ascii_lowercase
from Reflector import Reflector
from Rotor import Rotor
from Steckerbrett import Steckerbrett

class EnigmaMachine:
    def __init__(self, num_rotors):
        if not 3 <= num_rotors <= 8:
            raise ValueError("Le nombre de rotors doit être compris entre 3 et 8.")
        self.rotors = [Rotor() for _ in range(num_rotors)]
        self.reflector = Reflector()
        self.steckerbrett = Steckerbrett()

    def regler_position_rotor(self, positions):
        for rotor, position in zip(self.rotors, positions):
            rotor.regler_parametre(position)

    def pivoter_rotors(self):
        self.rotors[0].pivoter()
        if self.rotors[0].parametre == 0:
            self.rotors[1].pivoter()
            if self.rotors[1].parametre == 0:
                self.rotors[2].pivoter()

    def chiffrer_message(self, message):
        message_chiffre = ''
        for lettre in message.lower():
            if lettre in ascii_lowercase:
                lettre = self.steckerbrett.echanger(lettre)
                for rotor in self.rotors:
                    lettre = rotor.chiffrer_en_avant(lettre)
                lettre = self.reflector.refleter(lettre)
                for rotor in reversed(self.rotors):
                    lettre = rotor.chiffrer_en_arriere(lettre)
                lettre = self.steckerbrett.echanger(lettre)
                self.pivoter_rotors()
            message_chiffre += lettre
        return message_chiffre
    
    def afficher_configurations_rotors(self):
        for i, rotor in enumerate(self.rotors, 1):
            print(f"La configuration du rotor {i} est: {(rotor.afficher())}")
