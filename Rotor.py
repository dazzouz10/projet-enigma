# Rotor

from string import ascii_lowercase
import random
class Rotor:
    def __init__(self, parametre=0):
        self.parametre = parametre
        self.liaison = self.creer_liaison()

    def creer_liaison(self):
        alphabet = list(ascii_lowercase)
        random.shuffle(alphabet)
        return alphabet

    def regler_parametre(self, parametre):
        self.parametre = parametre

    def pivoter(self):
        self.parametre = (self.parametre + 1) % 26

    def chiffrer_en_avant(self, lettre):
        alphabet = list(ascii_lowercase)
        index = (alphabet.index(lettre) + self.parametre) % 26
        return self.liaison[index]

    def chiffrer_en_arriere(self, lettre):
        alphabet = list(ascii_lowercase)
        index = self.liaison.index(lettre)
        return alphabet[(index - self.parametre) % 26]
    
    def afficher(self):
        return self.parametre
    
#rotor = Rotor()
#pivot = rotor.pivoter()
#print(pivot)
#avant = rotor.chiffrer_en_avant('b')
#print(avant)
#arrière = rotor.chiffrer_en_arriere('c')
#print(arrière)
