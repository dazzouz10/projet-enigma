# Projet ENIGMA

Ce projet reprend le concept de la machine de chiffrement Enigma.

La machine Enigma est composé d'un certain nombre de rotors (entre 3 à 8 selon l'utilisateur), une planche de plugboard (Steckerbrett) et un réflecteur pour chiffrer et déchiffrer les messages.

---

## Installation

Il faut avoir Python d'installé sur votre machine. Puis créer un dossier à l'emplacement que vous souhaitez.

Ouvrir un terminal et positionnez-vous à l'emplacement du dossier : 

**Exemple** : Le nom du dossier est Enigma et se situe dans C:\Documents

```
cd C:\Documents\Enigma

```

Puis à l'aide de cette commande, vous pouvez cloner le dépôt sur le terminal : 
```
git clone https://gitlab.com/dazzouz10/projet-enigma.git

```
--- 

## Utilisation

Afin d'exécuter le projet, vous devez exécuter le fichier main.py avec la commande suivante :

```
python3 main.py

```
Saisir le nombre de rotors que vous voulez utiliser pour votre machine enigma (entre 3 à 8) sinon cela déclenchera une erreur.

Saisir le message que vous souhaitez chiffrer.

**Le message d'origine s'affichera ainsi chiffré et déchiffré(d'origine) si tout se passe bien**

---

#### Auteurs 
SANDIRASEGARANE Sabrina & AZZOUZ Damia  
