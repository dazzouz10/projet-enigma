# Reflector

from string import ascii_lowercase

class Reflector:
    def __init__(self):
        self.liaison = self.creer_liaison()

    def creer_liaison(self):
        alphabet = list(ascii_lowercase)
        return {alphabet[i]: alphabet[-(i+1)] for i in range(len(alphabet))}

    def refleter(self, lettre):
        return self.liaison[lettre]
    
#reflector = Reflector()
#lettre = reflector.refleter('a')
#print(lettre)
