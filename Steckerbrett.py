# Steckerbrett

from string import ascii_lowercase  
import random
class Steckerbrett:
    def __init__(self, nb_paires=10):
        self.liaison = self.initialiser_paires(nb_paires)

    def initialiser_paires(self, nb_paires):
        alphabet = list(ascii_lowercase)
        random.shuffle(alphabet)
        liaison = {}
        for i in range(nb_paires):
            if alphabet[i*2] not in liaison:
                liaison[alphabet[i*2]] = alphabet[i*2+1]
                liaison[alphabet[i*2+1]] = alphabet[i*2]
        print(liaison)
        return liaison

    def echanger(self, lettre):
        return self.liaison.get(lettre, lettre)

#steckerbrett = Steckerbrett()
#lettre = steckerbrett.echanger('a')
#print(lettre)

