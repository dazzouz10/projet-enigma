# main

import random
from EnigmaMachine import EnigmaMachine

if __name__ == "__main__":
    try:
        num_rotors = int(input("Saisissez le nombre de rotors (entre 3 et 8) : "))
    except ValueError:
        print("Entrée invalide. Veuillez entrer un numéro.")
        exit()

    if not 3 <= num_rotors <= 8:
        print("Le nombre de rotors doit être compris entre 3 et 8.")
        exit()

    enigma_machine = EnigmaMachine(num_rotors)

    initial_rotor_positions = [random.randint(0, 25) for _ in range(num_rotors)]
    enigma_machine.regler_position_rotor(initial_rotor_positions)
    enigma_machine.afficher_configurations_rotors()

    message_initial = input("Entrez le message à chiffrer: ")
    print(f"Message initial: {message_initial}")

    message_chiffre = enigma_machine.chiffrer_message(message_initial)
    print(f"Message chiffré: {message_chiffre}")

    enigma_machine.regler_position_rotor(initial_rotor_positions)

    message_dechiffre = enigma_machine.chiffrer_message(message_chiffre)
    print(f"Message dechiffré: {message_dechiffre}")
